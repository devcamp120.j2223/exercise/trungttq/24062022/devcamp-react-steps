import { Component } from "react";

class Steps extends Component{
    render(){
        let steps = this.props.step;
        return(
            <div>
                <ul>
                    {steps.map((element, index) => {
                        return <li key={index}>Bước {element.id}. {element.title}: {element.content} 
                        </li>
                    })}
                </ul>
            </div>
        )
    }
}
export default Steps;